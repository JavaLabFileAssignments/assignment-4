import java.util.Random;
import java.util.Scanner;
import cube.Cube;
import square.Square;

public class ThreadClass extends Thread{


    static Scanner scanner = new Scanner(System.in);
    public static void main(String args[])
    {
        ThreadClass  thread = new ThreadClass();
        thread.run();
    }

    public void run()
    {
        System.out.print("How many times do you want to execute thread: ");
        int times = scanner.nextInt();
        for(int i =0; i<times; i++)
        {
            Number number = new Number();
            int input = number.randomInput();
            Square s = new Square(input);
            s.start();
            Cube c = new Cube(input);
            c.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException exeption) {
                System.out.println(exeption);
            }
        }
    }
}

class Number extends Thread
{
    int randomNum;

    public int randomInput()
    {
        Random random = new Random();
        randomNum = random.nextInt(40);
        System.out.println("Random Integer generated around 40: " + randomNum);
        return randomNum;
    }


}

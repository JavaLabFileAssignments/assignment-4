package cube;

public class Cube extends Thread
{
    int cubeNum;
    public Cube(int cubeNum)
    {
        this.cubeNum = cubeNum;
    }
    public void run()
    {
        int cube = cubeNum * cubeNum * cubeNum;
        System.out.println("Cube of " + cubeNum + " = " + cube);
    }
}